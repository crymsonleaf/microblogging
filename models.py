from flask_security import RoleMixin, UserMixin
from peewee import *
import datetime

database = SqliteDatabase('data.sqlite3')

class BaseModel(Model):
    class Meta:
        database = database

class Role(BaseModel, RoleMixin):
    name = CharField(unique=True)
    description = TextField(null=True)

class User(BaseModel, UserMixin):
    username = CharField(unique=True)
    first_name = CharField()
    last_name = CharField()
    email = CharField(unique=True)
    password = TextField()
    active = BooleanField(default=True)
    creation_date = DateTimeField(default=datetime.datetime.now)

class UserRoles(BaseModel):
    user = ForeignKeyField(User, related_name='roles')
    role = ForeignKeyField(Role, related_name='users')
    name = property(lambda self: self.role.name)
    description = property(lambda self: self.role.description)

class Publication(BaseModel):
    title = CharField()
    body = TextField()
    creation_date = DateTimeField(default=datetime.datetime.now)
    update_date = DateTimeField(default=datetime.datetime.now)
    user = ForeignKeyField(User, backref='publications')


def create_tables():
    with database:
        database.create_tables([Role, User, UserRoles, Publication])

def drop_tables():
    with database:
        database.drop_tables([Role, User, UserRoles, Publication])