from flask import *
from flask_security import *
from models import *
from forms import *
import click

app = Flask(__name__)
app.config['SECRET_KEY'] = '123456'
app.config['SECURITY_PASSWORD_SALT'] = '654321'

"""base de donnée"""
user_datastore = PeeweeUserDatastore(database, User, Role, UserRoles)
security = Security(app, user_datastore)

@app.route('/')
def index():
    return render_template('posts/index.html', posts=Publication.select())

"""La route pour afficher la page de création de post
    mais on vérifie que l'utilsateur soit connecté au préalable
    sinon il est redirigé vers la page de connection """

@app.route('/posts/create', methods=['GET', 'POST'])
@login_required
def create():
    form = PostForm()
    if form.validate_on_submit():
        post = Publication.create(title=form.title.data, body=form.body.data, user=current_user.id)
        return redirect('/')
    return render_template('posts/create.html', form=form)

@app.route('/posts/edit/<string:id>', methods=['GET', 'POST'])
@login_required
def edit(id):
    form = PostForm()
    if form.validate_on_submit():
        post = Publication.get_by_id(id)
        post.title = form.title.data
        post.body = form.body.data
        post.save()
        flash('Post mis à jour')
        return redirect(url_for('edit.html'))
    return render_template('posts/edit.html', form=form)

@app.route('/posts/delete/<string:id>')
@login_required
def delete(id):
    post = Publication.get_by_id(id)
    post.delete_instance(id)
    flash('Post supprimer')
    return redirect(url_for('index'))



@app.route('/login', methods=['GET','POST'])
def login():
    form= LoginForm()
    if form.validate_on_submit():
        flash('Vous êtes connectée')
        return redirect(url_for('index.html'))
    return render_template('login.html', title = 'Sign In', form = form)

@app.route('/logout')
def logout():
    logout_user()
    flash('Vous êtes déconnecté')
    return redirect(url_for('posts/index.html'))

@app.route('/register', methods=['GET','POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegisterForm()
    if form.validate_on_submit():
        user_datastore.create_user(
            first_name = form.first_name.data,
            last_name = form.last_name.data,
            username = form.username.data,
            email = form.email.data,
            password = form.password.data)
        flash('Merci de vous être inscrit')
        return redirect(url_for('login'))
    return render_template('register.html', title = 'Register', form = form )

@app.cli.command()
def initdb():
    """Create database"""
    create_tables()
    click.echo('Initialized the database')

@app.cli.command()
def dropdb():
    """Drop database tables"""
    drop_tables()
    click.echo('Dropped tables from database')

@app.cli.command()
def fakedata():
    from faker import Faker
    fake = Faker()
    for _ in range(10):
        Publication.create(
            title=fake.sentence(),
            body=fake.text(),
            user=User.create(
                username=fake.email(),
                first_name=fake.first_name(),
                last_name=fake.last_name(),
                email=fake.email(),
                password="azert"
            )
        )